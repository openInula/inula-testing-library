import * as React from 'react'
import ReactDOM from 'react-dom'
import * as ReactDOMClient from 'react-dom/client'
import {
  getQueriesForElement,
  prettyDOM,
  configure as configureDTL,
} from '@testing-library/dom'
import act, {
  getIsReactActEnvironment,
  setReactActEnvironment,
} from './act-compat'
import { fireEvent } from './fire-event'
import { getConfig, configure } from './config'

/**
 * 检查jest的模拟定时器是否启用
 * 
 * 本函数旨在确定当前环境下是否使用了jest的模拟定时器这在测试环境中尤为重要，
 * 因为模拟定时器可以帮助我们更好地控制异步行为，而不受实际时间流逝的影响
 * 
 * @returns {boolean} 如果jest的模拟定时器已启用，则返回true；否则返回false
 */
function jestFakeTimersAreEnabled() {
  /* istanbul ignore else */ // 忽略此条件块以进行代码覆盖率计算，因为jest的定义状态会导致条件不总是可用
  if (typeof jest !== 'undefined' && jest !== null) { // 检查jest是否已定义且不为null，这通常意味着jest环境已被加载
    return (
      // legacy timers
      setTimeout._isMockFunction === true || // 检查setTimeout是否被标记为mock函数，这是旧版jest模拟定时器的一个标志
      // eslint-disable-next-line prefer-object-has-own -- No Object.hasOwn in all target environments we support.
      Object.prototype.hasOwnProperty.call(setTimeout, 'clock') // 检查setTimeout对象是否有'cock'属性，这是新版jest模拟定时器的标志
    )
  } /* istanbul ignore next */

  return false // 如果jest未定义或为null，则返回false，表示没有启用模拟定时器
}

// 配置DTL（Delay The Load）策略，用于优化测试环境下的异步操作和事件处理
configureDTL({
  // 包装不稳定的定时器操作，确保在非测试环境下正常运行
  unstable_advanceTimersWrapper: cb => {
    return act(cb)
  },
  // 定义异步操作的包装器，用于处理异步测试逻辑
  asyncWrapper: async cb => {
    // 保存当前的React Act环境状态
    const previousActEnvironment = getIsReactActEnvironment()
    // 设置React Act环境为false，以模拟非测试环境
    setReactActEnvironment(false)
    try {
      // 执行回调函数，并返回结果
      const result = await cb()
      // 清空微任务队列，以确保在resolve `waitFor`调用之前，恢复之前的Act环境
      await new Promise(resolve => {
        // 使用setTimeout来确保微任务队列被清空
        setTimeout(() => {
          resolve()
        }, 0)

        // 如果使用的是Jest假定时器，则前进定时器以清空队列
        if (jestFakeTimersAreEnabled()) {
          jest.advanceTimersByTime(0)
        }
      })

      return result
    } finally {
      // 恢复到原始的React Act环境状态
      setReactActEnvironment(previousActEnvironment)
    }
  },
  // 为事件处理提供包装，确保事件在Act环境下执行
  eventWrapper: cb => {
    let result
    // 在Act环境下执行回调，以确保事件立即执行
    act(() => {
      result = cb()
    })
    return result
  },
})

// Ideally we'd just use a WeakMap where containers are keys and roots are values.
// We use two variables so that we can bail out in constant time when we render with a new container (most common use case)
/**
 * @type {Set<import('react-dom').Container>}
 */
const mountedContainers = new Set()
/**
 * @type Array<{container: import('react-dom').Container, root: ReturnType<typeof createConcurrentRoot>}>
 */
const mountedRootEntries = []

function strictModeIfNeeded(innerElement) {
  return getConfig().reactStrictMode
    ? React.createElement(React.StrictMode, null, innerElement)
    : innerElement
}

function wrapUiIfNeeded(innerElement, wrapperComponent) {
  return wrapperComponent
    ? React.createElement(wrapperComponent, null, innerElement)
    : innerElement
}

function createConcurrentRoot(
  container,
  { hydrate, ui, wrapper: WrapperComponent },
) {
  let root
  if (hydrate) {
    act(() => {
      root = ReactDOMClient.hydrateRoot(
        container,
        strictModeIfNeeded(wrapUiIfNeeded(ui, WrapperComponent)),
      )
    })
  } else {
    root = ReactDOMClient.createRoot(container)
  }

  return {
    hydrate() {
      /* istanbul ignore if */
      if (!hydrate) {
        throw new Error(
          'Attempted to hydrate a non-hydrateable root. This is a bug in `@testing-library/react`.',
        )
      }
      // Nothing to do since hydration happens when creating the root object.
    },
    render(element) {
      root.render(element)
    },
    unmount() {
      root.unmount()
    },
  }
}

function createLegacyRoot(container) {
  return {
    hydrate(element) {
      ReactDOM.hydrate(element, container)
    },
    render(element) {
      ReactDOM.render(element, container)
    },
    unmount() {
      ReactDOM.unmountComponentAtNode(container)
    },
  }
}

function renderRoot(
  ui,
  { baseElement, container, hydrate, queries, root, wrapper: WrapperComponent },
) {
  act(() => {
    if (hydrate) {
      root.hydrate(
        strictModeIfNeeded(wrapUiIfNeeded(ui, WrapperComponent)),
        container,
      )
    } else {
      root.render(
        strictModeIfNeeded(wrapUiIfNeeded(ui, WrapperComponent)),
        container,
      )
    }
  })

  return {
    container,
    baseElement,
    debug: (el = baseElement, maxLength, options) =>
      Array.isArray(el)
        ? // eslint-disable-next-line no-console
        el.forEach(e => console.log(prettyDOM(e, maxLength, options)))
        : // eslint-disable-next-line no-console,
        console.log(prettyDOM(el, maxLength, options)),
    unmount: () => {
      act(() => {
        root.unmount()
      })
    },
    rerender: rerenderUi => {
      renderRoot(rerenderUi, {
        container,
        baseElement,
        root,
        wrapper: WrapperComponent,
      })
      // Intentionally do not return anything to avoid unnecessarily complicating the API.
      // folks can use all the same utilities we return in the first place that are bound to the container
    },
    asFragment: () => {
      /* istanbul ignore else (old jsdom limitation) */
      if (typeof document.createRange === 'function') {
        return document
          .createRange()
          .createContextualFragment(container.innerHTML)
      } else {
        const template = document.createElement('template')
        template.innerHTML = container.innerHTML
        return template.content
      }
    },
    ...getQueriesForElement(baseElement, queries),
  }
}

function render(
  ui,
  {
    container,
    baseElement = container,
    legacyRoot = false,
    queries,
    hydrate = false,
    wrapper,
  } = {},
) {
  if (legacyRoot && typeof ReactDOM.render !== 'function') {
    const error = new Error(
      '`legacyRoot: true` is not supported in this version of React. ' +
      'If your app runs React 19 or later, you should remove this flag. ' +
      'If your app runs React 18 or earlier, visit https://react.dev/blog/2022/03/08/react-18-upgrade-guide for upgrade instructions.',
    )
    Error.captureStackTrace(error, render)
    throw error
  }

  if (!baseElement) {
    // default to document.body instead of documentElement to avoid output of potentially-large
    // head elements (such as JSS style blocks) in debug output
    baseElement = document.body
  }
  if (!container) {
    container = baseElement.appendChild(document.createElement('div'))
  }

  let root
  // eslint-disable-next-line no-negated-condition -- we want to map the evolution of this over time. The root is created first. Only later is it re-used so we don't want to read the case that happens later first.
  if (!mountedContainers.has(container)) {
    const createRootImpl = legacyRoot ? createLegacyRoot : createConcurrentRoot
    root = createRootImpl(container, { hydrate, ui, wrapper })

    mountedRootEntries.push({ container, root })
    // we'll add it to the mounted containers regardless of whether it's actually
    // added to document.body so the cleanup method works regardless of whether
    // they're passing us a custom container or not.
    mountedContainers.add(container)
  } else {
    mountedRootEntries.forEach(rootEntry => {
      // Else is unreachable since `mountedContainers` has the `container`.
      // Only reachable if one would accidentally add the container to `mountedContainers` but not the root to `mountedRootEntries`
      /* istanbul ignore else */
      if (rootEntry.container === container) {
        root = rootEntry.root
      }
    })
  }

  return renderRoot(ui, {
    container,
    baseElement,
    queries,
    hydrate,
    wrapper,
    root,
  })
}

function cleanup() {
  mountedRootEntries.forEach(({ root, container }) => {
    act(() => {
      root.unmount()
    })
    if (container.parentNode === document.body) {
      document.body.removeChild(container)
    }
  })
  mountedRootEntries.length = 0
  mountedContainers.clear()
}

function renderHook(renderCallback, options = {}) {
  const { initialProps, ...renderOptions } = options

  if (renderOptions.legacyRoot && typeof ReactDOM.render !== 'function') {
    const error = new Error(
      '`legacyRoot: true` is not supported in this version of React. ' +
      'If your app runs React 19 or later, you should remove this flag. ' +
      'If your app runs React 18 or earlier, visit https://react.dev/blog/2022/03/08/react-18-upgrade-guide for upgrade instructions.',
    )
    Error.captureStackTrace(error, renderHook)
    throw error
  }

  const result = React.createRef()

  function TestComponent({ renderCallbackProps }) {
    const pendingResult = renderCallback(renderCallbackProps)

    React.useEffect(() => {
      result.current = pendingResult
    })

    return null
  }

  const { rerender: baseRerender, unmount } = render(
    <TestComponent renderCallbackProps={initialProps} />,
    renderOptions,
  )

  function rerender(rerenderCallbackProps) {
    return baseRerender(
      <TestComponent renderCallbackProps={rerenderCallbackProps} />,
    )
  }

  return { result, rerender, unmount }
}

// just re-export everything from dom-testing-library
export * from '@testing-library/dom'
export { render, renderHook, cleanup, act, fireEvent, getConfig, configure }

/* eslint func-name-matching:0 */
