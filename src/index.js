import * as rtlAPI from './rtl'

const { render, renderHook, cleanup, act, fireEvent, getConfig, configure } = rtlAPI

export * from '@testing-library/dom'
export { render, renderHook, cleanup, act, fireEvent, getConfig, configure }
