import React from "react";
import { queries as defaultQueries } from "@testing-library/dom";
import * as ReactDOMClient from "react-dom/client";

export interface RenderOptions {
  container?: HTMLElement;
  baseElement?: HTMLElement;
  legacyRoot?: boolean;
  queries?: typeof defaultQueries;
  hydrate?: boolean;
  wrapper?: React.ComponentType;
}

export interface RenderRootOptions {
  baseElement?: HTMLElement;
  container?: HTMLElement;
  hydrate?: boolean;
  queries?: typeof defaultQueries;
  root: ReactDOMClient.Root;
  wrapper?: React.ComponentType;
}

export interface RenderResult {
  container: HTMLElement;
  baseElement: HTMLElement;
  debug: (
    el?: HTMLElement | HTMLElement[],
    maxLength?: number,
    options?: object
  ) => void;
  unmount: () => void;
  rerender: (rerenderUi: React.ReactElement) => void;
  asFragment: () => DocumentFragment;
  [key: string]: any;
}

export function strictModeIfNeeded(ui: React.ReactElement): React.ReactElement {
  if (process.env.NODE_ENV === "production") {
    return ui;
  }
  return <React.StrictMode>{ui}</React.StrictMode>;
}

export function wrapUiIfNeeded(
  ui: React.ReactElement,
  WrapperComponent?: React.ComponentType
): React.ReactElement {
  if (!WrapperComponent) {
    return ui;
  }
  return <WrapperComponent>{ui}</WrapperComponent>;
}
